<?php
/**
Template Name: Landing Page
Created by PhpStorm.
User: MEO
Date: 29.03.2017
Time: 10:24
 */

global $post, $mk_options;
$post_thumbnail_id = get_post_thumbnail_id( $post );

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
$context['post'] = $post;
$context['count'] = 1;
$context['year'] = date('Y');
$context['get_content_blocs'] = get_field('content_bloc');
$context['has_sub_field'] = has_sub_field('content_bloc');
$context['get_list_logos'] = get_field('list_logos');
$context['has_sub_list_logos'] = has_sub_field('list_logos');
$context['featured_image'] = wp_get_attachment_image_url( $post_thumbnail_id, 'full' );
$context['theme_directory_uri'] = get_template_directory_uri();

/*
 * TXT TRADUCTION
 */
$context['label_vercorin'] = __('[:fr]En savoir plus sur le projet[:en]Learn more about the project[:de]Erfahren Sie mehr über das Projekt');
$context['label_pradas'] = __('[:fr]Réservez votre séjour maintenant[:en]Book your stay now[:de]Buchen Sie jetzt Ihren Aufenthalt');


$context['language_switcher'] = Timber::get_widgets('language_switcher');

Timber::render( 'landing.twig' , $context );