<?php

class MeoCrmCoreExternalDbQuery
{
    public static function select($external_wpdb, $query, $type_array = ARRAY_A)
    {
        $resultQuery = $external_wpdb->get_results( $query, $type_array );

        return $resultQuery;
    }

    public static function insert($external_wpdb, $table, $data)
    {
        if($external_wpdb->insert($table,$data))
        {
            return $external_wpdb->last_id;
        }
        return false;
    }

    public static function update($external_wpdb,$table,$data,$where)
    {
        if($external_wpdb->update($table,$data,$where))
        {
            return true;
        }
        return false;
    }

    public static function delete($external_wpdb,$table,$where)
    {
        if($external_wpdb->delete($table,$where))
        {
            return true;
        }
        return false;
    }
}